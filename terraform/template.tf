provider "aws" {
  region  = "us-west-2"
  profile = var.aws_profile
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

variable "stage_name" {
  type        = string
  default     = "dev"
  description = "Name of the API Gateway Stage to which the API will be deployed."
}

variable "aws_profile" {
type = string
default = "default"
description = "AWS profile to be used when creating resources"
}

# Base API Gateway API resource
resource "aws_api_gateway_rest_api" "rest_api" {
  name        = "terraform-example-api"
  description = "Simple hello world REST API deployed via Terraform"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

# Deploys API Gateway resources to the given stage
resource "aws_api_gateway_deployment" "rest_api_deployment" {
  depends_on = [
    aws_api_gateway_integration.lambda_proxy_integration_get,
    aws_api_gateway_integration.lambda_proxy_integration_post
  ]
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  stage_name  = var.stage_name
}

# Creates an API resource at the "/hello" path
resource "aws_api_gateway_resource" "lambda_proxy_resource" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  parent_id   = aws_api_gateway_rest_api.rest_api.root_resource_id
  path_part   = "hello"
}

# Creates a GET endpoint for the "/hello" resource
resource "aws_api_gateway_method" "lambda_proxy_method_get" {
  rest_api_id   = aws_api_gateway_rest_api.rest_api.id
  resource_id   = aws_api_gateway_resource.lambda_proxy_resource.id
  http_method   = "GET"
  authorization = "NONE"
}

# Integration resource to point the endpoint at the correct Lambda
resource "aws_api_gateway_integration" "lambda_proxy_integration_get" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.lambda_proxy_resource.id
  http_method = aws_api_gateway_method.lambda_proxy_method_get.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_function.invoke_arn
}

# Creates a POST endpoint for the "/hello" resource
resource "aws_api_gateway_method" "lambda_proxy_method_post" {
  rest_api_id   = aws_api_gateway_rest_api.rest_api.id
  resource_id   = aws_api_gateway_resource.lambda_proxy_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

# Integration resource to point the endpoint at the correct Lambda
resource "aws_api_gateway_integration" "lambda_proxy_integration_post" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.lambda_proxy_resource.id
  http_method = aws_api_gateway_method.lambda_proxy_method_post.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_function.invoke_arn
}

# Creates a zip archive of the Lambda source code for upload
data "archive_file" "lambda_source" {
  type        = "zip"
  source_file = "${path.module}/../lambda/function.py"
  output_path = "${path.module}/../lambda/function.zip"
}

# Deploys the hello-world Python Lambda function
resource "aws_lambda_function" "lambda_function" {
  function_name = "terraform_hello_world"

  filename = data.archive_file.lambda_source.output_path
  source_code_hash = data.archive_file.lambda_source.output_base64sha256

  handler     = "function.handler"
  runtime     = "python3.8"
  memory_size = 128
  timeout     = 5

  role = aws_iam_role.lambda_role.arn
}

# Allows API Gateway resources to invoke the Lambda
resource "aws_lambda_permission" "invoke_permission" {
  statement_id  = "APIGatewayLambdaInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.rest_api.execution_arn}/${var.stage_name}/*"

}

# Lambda IAM Role
resource "aws_iam_role" "lambda_role" {
  name = "lambda_role"

  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy.json
}

# Lambda assume role policy statement
data "aws_iam_policy_document" "lambda_assume_role_policy" {
  statement {
    sid     = "LambdaAssumeRole"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

# Attach Policy to Role
resource "aws_iam_role_policy_attachment" "lambda_policy_attachment" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaExecute"
}
