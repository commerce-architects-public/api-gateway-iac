# API Gateway IaC Examples

## Description
This repo contains the Python Lambda and IaC templates used to deploy a simple hello world API in AWS API Gateway via Terraform, CloudFormation, and AWS SAM.

For more information please see the accompanying blog post: __BLOG LINK TBD__

## Links
#### IaC Install/Deployment
* [Terraform](https://learn.hashicorp.com/collections/terraform/aws-get-started)
* [CloudFormation](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/GettingStarted.Walkthrough.html)
* [SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-getting-started.html)

#### AWS Resources
* [Lambda](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
* [API Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/welcome.html)ß