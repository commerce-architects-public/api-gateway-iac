import json

def handler(event, context):
    print('Received event: ' + json.dumps(event, indent=2))
    
    method = event['httpMethod']

    if method == 'GET':
        response = get()
    elif method == 'POST':
        response = post()
    else:
        response = {
            'statusCode': 400,
            'body': json.dumps({
                'error': 'HTTP method not supported'
            })
        }

    return response

def get():
    return {
        'statusCode': 200,
        'body': 'GET response'
    }

def post():
    return {
        'statusCode': 200,
        'body': 'POST response'
    }